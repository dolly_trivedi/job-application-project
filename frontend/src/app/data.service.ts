import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }


  getData(){
    return this.http.get('http://127.0.0.1:8000/api/employee');
  }

  addEmployeeData(data){
    return this.http.post('http://127.0.0.1:8000/api/addemp',data)
  }

  deleteEmployee(id){
    return this.http.delete("http://127.0.0.1:8000/api/deleteemp/"+id);
  }

  getOneEmployee(id){
    return this.http.get("http://127.0.0.1:8000/api/employee/"+id);
  }

  updateEmployee(id,data){
    return this.http.put("http://127.0.0.1:8000/api/editemp/"+id,data);
  }

  postLogin(data){
    return this.http.post("http://127.0.0.1:8000/api/login",data);
  }
  // dropdowndemo(){
  //   return this.http.get("http://127.0.0.1:8000/api/dropdown-demo");
  // }

  products(){
    return [{
      id:1,
      name:'Laptop'
    },
    {
      id:2,
      name:'Digital Watch'
    }]
  }

  productsOfProduct(){
    return this.http.get("http://127.0.0.1:8000/api/dropdown-demo");
  //   return [{
  //     id:1,
  //     name:'Dell'
  //   },
  //   {
  //     id:1,
  //     name:'Lenovo'
  //   },
  //   {
  //     id:1,
  //     name:'HP'
  //   },
  //   {
  //     id:2,
  //     name:'Noise'
  //   },
  //   {
  //     id:2,
  //     name:'Boat'
  //   },
  // ]
  }
}
