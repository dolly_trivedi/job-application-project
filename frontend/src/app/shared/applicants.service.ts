import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApplicantsService {

  constructor(private http:HttpClient) { }

  addApplicants(data){
    return this.http.post('http://127.0.0.1:8000/api/add-applicants',data);
  }
  getApplicants(){
    return this.http.get('http://127.0.0.1:8000/api/applicant-list');
  }
  deleteApplicants(id){
    return this.http.delete('http://127.0.0.1:8000/api/delete-applicant/'+id);
  }
  viewApplicant(id){
    return this.http.get('http://127.0.0.1:8000/api/applicant/'+id);
  }
}
