import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators, FormControl } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  constructor(private fb:FormBuilder,
    private dataService:DataService,
    private route:Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email' : ['',[Validators.required,Validators.email]],
      'password':['',Validators.required]
    });
  }
  checkLogin(){
    this.dataService.postLogin(this.loginForm.value).subscribe(
      (response: any) => {
        if(response.status === 200)
         {
            this.route.navigate(["applicant-list"]);
          }
      },
      (error) => {
        if (error.status === 400) {
          this.route.navigate([""]);
          console.log(error);
        }
      }
    );
  }
  }

