import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import { Employee } from "./employee.model";

@Component({
  selector: "app-employee",
  templateUrl: "./employee.component.html",
  styleUrls: ["./employee.component.css"],
})
export class EmployeeComponent implements OnInit {
  constructor(private dataService: DataService) {}

  dataArr: any;
  employee = new Employee();
  ngOnInit() {
    this.getEmployeeData();
  }

  getEmployeeData() {
    this.dataService.getData().subscribe((res) => {
      this.dataArr = res;
    });
  }

  insertData() {
   this.dataService.addEmployeeData(this.employee).subscribe((res)=>{
    this.getEmployeeData();
   })
  }
  deleteEmployee(id){
    this.dataService.deleteEmployee(id).subscribe((res=>{
      this.getEmployeeData();
    }))
  }
}
