import { Component, OnInit } from '@angular/core';
import { ApplicantsService } from '../shared/applicants.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-applicant-view',
  templateUrl: './applicant-view.component.html',
  styleUrls: ['./applicant-view.component.css']
})
export class ApplicantViewComponent implements OnInit {
  id:number;
  applicantArr:any;
  constructor(private api:ApplicantsService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.getApplicantById();
  }

  getApplicantById(){
   this.id = this.route.snapshot.params.id;
    this.api.viewApplicant(this.id).subscribe((res)=>{
      this.applicantArr = res;
    })
  }
}
