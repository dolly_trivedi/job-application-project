import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { LoginComponent } from './login/login.component';
import { ApplicantsComponent } from './applicants/applicants.component';
import { ApplicantListComponent } from './applicant-list/applicant-list.component';
import { ApplicantViewComponent } from './applicant-view/applicant-view.component';
import { DropdownDemoComponent } from './dropdown-demo/dropdown-demo.component';


const routes: Routes = [
  { path: '', component:LoginComponent },
  {path:'employee',component:EmployeeComponent},
  { path: 'edit/:id', component: EditEmployeeComponent },
  { path: 'applicants-admin', component: ApplicantsComponent },
  {path:'applicant-list',component:ApplicantListComponent},
  {path:'applicant-view/:id',component:ApplicantViewComponent},
  {path:'dropdown-demo',component:DropdownDemoComponent}

];



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
})
export class AppRoutingModule { }
