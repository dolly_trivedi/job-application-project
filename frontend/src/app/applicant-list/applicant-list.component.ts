import { Component, OnInit } from '@angular/core';
import { ApplicantsService } from '../shared/applicants.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applicant-list',
  templateUrl: './applicant-list.component.html',
  styleUrls: ['./applicant-list.component.css']
})
export class ApplicantListComponent implements OnInit {

  constructor(private apiService:ApplicantsService,private route:Router) { }
  dataArr:any;
  ngOnInit() {
    this.getAllApplicants();
  }
  getAllApplicants(){
    this.apiService.getApplicants().subscribe((res=>{
      this.dataArr = res;
    }))
  }
  deleteApplicantions(id){
    this.apiService.deleteApplicants(id).subscribe((res=>{
      console.log(res);
      this.getAllApplicants();
    }))
  }
  viewApplications(id){
    this.route.navigate(['applicant-view/'+id]);
    
  }
}
