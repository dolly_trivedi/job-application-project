import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ApplicantsComponent } from './applicants/applicants.component';
import { ApplicantListComponent } from './applicant-list/applicant-list.component';
import { ApplicantViewComponent } from './applicant-view/applicant-view.component';
import { DropdownDemoComponent } from './dropdown-demo/dropdown-demo.component';
@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    EditEmployeeComponent,
    LoginComponent,
    ApplicantsComponent,
    ApplicantListComponent,
    ApplicantViewComponent,
    DropdownDemoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
