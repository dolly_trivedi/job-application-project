import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup,Validator, Validators } from '@angular/forms';
import { ApplicantsService } from '../shared/applicants.service';

@Component({
  selector: "app-applicants",
  templateUrl: "./applicants.component.html",
  styleUrls: ["./applicants.component.css"],
})
export class ApplicantsComponent implements OnInit {
  productForm: FormGroup;
  addApp: FormGroup;
  languageForm: FormGroup;
  langLevelForm:FormGroup;
  techExp: FormGroup;
  isLaravel = "";
  isNode = "";
  isReact = "";
  data: any;
  constructor(private fb: FormBuilder, private apiService: ApplicantsService) {
    this.productForm = this.fb.group({
      applicantExp: this.fb.array([]),
    });
  }
  knownLanguages: Array<any> = [
    {
      name: "Hindi",
      value: "1",
      levels: [
        {
          name: "Read",
          value: 1,
        },
        {
          name: "Write",
          value: 2,
        },
        {
          name: "Speak",
          value: 3,
        },
      ]
    },
    {
      name: "English",
      value: "2",
      levels: [
        {
          name: "Read",
          value: 1,
        },
        {
          name: "Write",
          value: 2,
        },
        {
          name: "Speak",
          value: 3,
        },
      ]
    },
    {
      name: "Gujrati",
      value: "3",
      levels: [
        {
          name: "Read",
          value: 1,
        },
        {
          name: "Write",
          value: 2,
        },
        {
          name: "Speak",
          value: 3,
        },
      ]
    },
  ];
  // levels: Array<any> = [
  //   {
  //     name: "Read",
  //     value: 1,
  //   },
  //   {
  //     name: "Write",
  //     value: 2,
  //   },
  //   {
  //     name: "Speak",
  //     value: 3,
  //   },
  // ];
  selectedLanguages = [];
  selectedLanguageLeavels = [];

  ngOnInit() {
    this.addApp = this.fb.group({
      name: [""],
      email: [""],
      contact_num: [""],
      address: [""],
      gender: [""],
      ssc_result: [""],
      hsc_result: [""],
      graduation: [""],
      master_degree: [""],
      board_uni: [""],
      year: [""],
      cgpa: [""],
      expected_ctc: [""],
      current_ctc: [""],
      location: [""],
      notice_period: [""],
    });
    this.techExp = this.fb.group({
      2: [""],
      1: [""],
      3: [""],
    });
    console.log(this.knownLanguages.map((x)=>(x.levels.map((y)=>(y.value)))))
    this.languageForm = this.fb.group({
      languages: this.fb.array(this.knownLanguages.map((x)=>(x.value)>0)),
      levels: this.fb.array([]),
    });
  }

  addExperience(): FormArray {
    return this.productForm.get("applicantExp") as FormArray;
  }
  newExperience(): FormGroup {
    return this.fb.group({
      company: "",
      designation: "",
      from: "",
      to: "",
    });
  }
  addQuantity() {
    this.addExperience().push(this.newExperience());
  }

  removeQuantity(i: number) {
    this.addExperience().removeAt(i);
  }

  checkValue(event) {
    console.log(event);
  }

  insertApplicants() {
    const appForm = this.addApp.value;
    const expForm = this.productForm.value["applicantExp"][0];
    const techForm = this.techExp.value;
    const langForm = this.selectedLanguages;
    this.data = {
      appForm,
      expForm,
      techForm,
      langForm,
    };

    this.apiService.addApplicants(this.data).subscribe((res) => {
      this.addApp.reset();
    });
  }
  onChange(e) {
    if (e.target.checked) {
      this.selectedLanguages.push(e.target.value);
    } else {
      const index: number = this.selectedLanguages.indexOf(e.target.value);
      if (index !== -1) {
          this.selectedLanguages.splice(index, 1);
      }
    }
    console.log(this.selectedLanguages);
  }
}
