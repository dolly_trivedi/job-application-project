import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dropdown-demo',
  templateUrl: './dropdown-demo.component.html',
  styleUrls: ['./dropdown-demo.component.css']
})
export class DropdownDemoComponent implements OnInit {

  constructor(private data:DataService) { }
  products:any=[];
  productsOfProduct:any=[];
  isProductSelect:boolean = false;
  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    this.products = this.data.products();
   
  }
  onChangeProducts(event){
    this.data.productsOfProduct().subscribe((res)=>{
      this.productsOfProduct = res;
      // this.productsOfProduct = this.productsOfProduct.filter(e=>e.prod_id == event.target.value);
      this.isProductSelect = true;
      console.log(this.productsOfProduct)
    })
    // this.productsOfProduct = this.data.productsOfProduct().filter(e => e.id == event.target.value);
    // this.isProductSelect = true;
  }
}
