import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { Employee } from '../employee/employee.model';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  empId:any;
  updateDataArr:any;
  employee = new Employee();
  constructor(private route:ActivatedRoute,private dataService: DataService) { }

  ngOnInit() {
  this.empId = this.route.snapshot.params.id;
  this.editData();
  }

  editData(){
    this.dataService.getOneEmployee(this.empId).subscribe((res=>{
      this.updateDataArr = res;
      this.employee = this.updateDataArr;
       }))
  }
  updateData(){
    this.dataService.updateEmployee(this.empId,this.employee).subscribe((res=>{
      console.log(res);
    }))
  }
}
