<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TechnicalKnoledge extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'technical_knoledge';
    protected $guarded = ['id'];
}
