<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsOfProduct extends Model
{
    use HasFactory;
    protected $table ='productslist';

    public function products(){
        return $this->belongsTo('App\Models\Product','prod_id','id');
    }
}
