<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TechnicalLanguage extends Model
{
    use HasFactory;
    protected $table = 'technical_language';
    protected $guarded = ['id'];
}
