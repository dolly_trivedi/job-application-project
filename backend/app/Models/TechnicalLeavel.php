<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TechnicalLeavel extends Model
{
    use HasFactory;
    protected $table = 'technical_leavel';
    protected $guarded = ['id'];
}
