<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employee';
    protected $guarded = ['id'];
    public $timestamps = false;
    public function getEmployee()
    {
       $data= DB::table('employee')->get();
       return $data;
    }

    public function addEmployee($data)
    {
        DB::table('employee')->insert($data);
    }

    public function deleteEmployeeData($id)
    {
        DB::table('employee')->delete($id);
    }
    public function updateEmployee($id,$data)
    {
        DB::table('employee')->where('id',$id)->update($data);
    }
    public function getEmployeeById($id)
    {
        $data = DB::table('employee')->where('id',$id)->get()->first();
        return $data;
    }
}
