<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkExperience extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'work_experience';
    protected $guarded = ['id'];

    public function applicants(){
        return $this->belongsTo('App\Models\Applicant','app_id','id');
    }
}
