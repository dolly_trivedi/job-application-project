<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NestedProduct extends Model
{
    use HasFactory;
    protected $table = 'nested_products';
    protected $guarded = ['id'];

    public function productsOfProduct(){
        return $this->belongsTo('App\Models\ProductsOfProduct','nest_prod_id','id');
    }
}
