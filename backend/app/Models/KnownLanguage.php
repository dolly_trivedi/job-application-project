<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KnownLanguage extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'known_languages';
    protected $guarded = ['id'];
}
