<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function postLogin(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required'
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
           return response()->json([
            'status'=>200,
            'data'=> $credentials
           ]);
        }
        else{
            return response()->json([
                'status'=>400,
                'message'=>"Sorry You Can't Login"
               ]);
        }

    }
}
