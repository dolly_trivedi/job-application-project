<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\WorkExperience;
use App\Models\KnownLanguage;
use App\Models\Language;
use App\Models\TechnicalKnoledge;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApplicantsController extends Controller
{
    public function addApplicants(Request $request)
    {
       
        $app_form = $request['appForm'];
        $exp_form = $request['expForm'];
        $tech_form= $request['techForm'];
        
        $app = new Applicant();
        $app->name = $app_form['name'];
        $app->email = $app_form['email'];
        $app->address = $app_form['address'];
        $app->gender = $app_form['gender'];
        $app->contact_num = $app_form['contact_num'];
        $app->ssc_result = $app_form['ssc_result'];
        $app->hsc_result = $app_form['hsc_result'];
        $app->graduation = $app_form['graduation'];
        $app->master_degree = $app_form['master_degree'];
        $app->year = $app_form['year'];
        $app->board_uni = $app_form['board_uni'];
        $app->cgpa= $app_form['cgpa'];
        $app->location = $app_form['location'];
        $app->expected_ctc = $app_form['expected_ctc'];
        $app->current_ctc = $app_form['current_ctc'];
        $app->notice_period = $app_form['notice_period'];
        
         $app->save();
       

        // Applicant::create([
        //     'name'=>$request->name,
        //     'email'=>$request->email,
        //     'address'=> $request->address,
        //     'gender' => $request->gender,
        //     'contact_num' => $request->contact_num,
        //     'ssc_result' => $request->ssc_result,
        //     'hsc_result' => $request->hsc_result,
        //     'graduation' => $request->graduation,
        //     'master_degree' => $request->master_degree,
        //     'year' => $request->year,
        //     'board_uni' => $request->board_uni,
        //     'cgpa'=> $request->cgpa,
        //     'location' => $request->location,
        //     'expected_ctc' => $request->expected_ctc,
        //     'current_ctc' => $request->current_ctc,
        //     'notice_period' => $request->notice_period
        //  ]);

        $appExp= new WorkExperience();
        $appExp->app_id = $app->id;
        $appExp->company = $exp_form['company'];
        $appExp->designation=$exp_form['designation'];
        $appExp->from_exp= date('Y-m-d', strtotime($exp_form['from']));
        $appExp->to_exp= date('Y-m-d', strtotime($exp_form['to']));
        $appExp->save();
        
         
        // foreach ($request->language as $key => $value) {
        //     $languagedata = implode(",",$value);
        //     $userlanguage =  KnownLanguage::create([
        //         'app_id' => $app->id,
        //         'language_id' => $key,
        //         'language_proficiency_id' =>$languagedata
        //     ]);
        // }

        foreach ($tech_form as $key => $value) {
            // $techdata = implode("",$value);
            $technicalExp = TechnicalKnoledge::create([
                'app_id' => $app->id,
                'technical_language_id' => $key,
                'technical_leavel_id' =>$value
            ]);
            // dd($key);
            // dd($value);
        } 
          
    
    return response()->json([
        'status'=>200,
        'message'=>"data saved to the database"
    ]);

    }
    public function deleteApplicant($id)
    {
         DB::table("known_languages")->where("app_id", $id)->delete();
         DB::table("technical_knoledge")->where("app_id", $id)->delete();
         DB::table("work_experience")->where("app_id",$id)->delete();
        //  WorkExperience::destroy("app_id",$id);
         DB::table("applicants")->where("id", $id)->delete();
      return response()->json([
        'status'=>200,
        'message'=>"data deleted from database"
    ]);
    }

    public function getAllApplicants()
    {
        $users = DB::table('applicants')->get();    
        return $users;
    }
    public function getApplicantById($id,Request $req)
    {
      $workExp = WorkExperience::with('applicants')->where('app_id',$id)->get();
      return $workExp;
    }
}
