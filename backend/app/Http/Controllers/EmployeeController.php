<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Product;
use App\Models\ProductsOfProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\NestedProduct;

class EmployeeController extends Controller
{
    public function getData()
    {
       $employeeModel = new Employee();
      $data= $employeeModel ->getEmployee();
      return response()->json($data);
    }
    public function addEmployeeData(Request $req)
    {
        $employeeModel = new Employee();
        $data= $employeeModel ->addEmployee($req->all());
    }

    public function deleteEmployee(Request $req)
    {
      $id = $req->id;
      $employeeModel = new Employee();
      $employeeModel ->deleteEmployeeData($id);
    }
    public function editEmployee(Request $req)
    {
        $id = $req->id;
        $employeeModel = new Employee();
        $employeeModel ->updateEmployee($id,$req->all());
    }
    public function getDataById($id,Request $req)
    {
      $id = $req->id;
      $employeeModel= new Employee();
     $data= $employeeModel->getEmployeeById($id);
     return response()->json($data);
    }
    public function getProducts()
    {
      $products = ProductsOfProduct::with('products')->get();
      $productsList = NestedProduct::with('productsOfProduct')->get();

      return response()->json(array(
    'products' => $products,
    'productsList' => $productsList
));
    }
}
