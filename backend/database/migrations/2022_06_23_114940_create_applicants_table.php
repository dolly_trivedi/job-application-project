<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->text('address');
            $table->string('gender');
            $table->string('contact_num');
            $table->float('ssc_result', 15, 2)->nullable();
            $table->float('hsc_result', 15, 2)->nullable();
            $table->float('graduation', 15, 2)->nullable();
            $table->float('master_degree', 15, 2)->nullable();
            $table->text('board_uni')->nullable();
            $table->integer('year')->nullable();
            $table->integer('cgpa')->nullable();
            $table->string('location')->nullable();
            $table->float('expected_ctc', 15, 2)->nullable();
            $table->float('current_ctc', 15, 2)->nullable();
            $table->integer('notice_period')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
