<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('address');
            $table->string('gender');
            $table->string('contact_num');
            $table->string('ssc_result')->nullable();
            $table->string('hsc_result')->nullable();
            $table->string('graduation')->nullable();
            $table->string('master_degree')->nullable();
            $table->string('board_uni')->nullable();
            $table->integer('year')->nullable();
            $table->integer('cgpa')->nullable();
            $table->string('location')->nullable();
            $table->float('expected_ctc', 15, 2)->nullable();
            $table->float('current_ctc', 15, 2)->nullable();
            $table->integer('notice_period')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
