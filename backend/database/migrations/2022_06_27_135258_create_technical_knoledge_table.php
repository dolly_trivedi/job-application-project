<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnicalKnoledgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technical_knoledge', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('app_id');
            $table->foreign('app_id')->references('id')->on('applicants');
            $table->unsignedBigInteger('technical_language_id');
            $table->foreign('technical_language_id')->references('id')->on('technical_language');
            $table->unsignedBigInteger('technical_leavel_id');
            $table->foreign('technical_leavel_id')->references('id')->on('technical_leavel');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technical_knoledge');
    }
}
