<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TechnicalLanguage;

class TechnicalLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TechnicalLanguage::create([
            'language' => 'Laravel',
        ]);
    }
}
