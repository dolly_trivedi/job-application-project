<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'dolly',
            'email' => 'admin1@yopmail.com',
            'password' => Hash::make('123456'),
            'role_id'=>1
        ]);
    }
}
