<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TechnicalLeavel;

class TechnicalLeavelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TechnicalLeavel::create([
            'leavel' => 'Expert'
        ]);
    }
}
