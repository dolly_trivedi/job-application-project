<?php

namespace Database\Seeders;

use App\Models\ProductsOfProduct;
use Illuminate\Database\Seeder;

class ProductsListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductsOfProduct::create([
            'name' => 'Dell',
            'prod_id'=>1
        ]);
        ProductsOfProduct::create([
            'name' => 'Lenovo',
            'prod_id'=>1
        ]);
        ProductsOfProduct::create([
            'name' => 'HP',
            'prod_id'=>1
        ]);
        ProductsOfProduct::create([
            'name' => 'Noise',
            'prod_id'=>2
        ]);
        ProductsOfProduct::create([
            'name' => 'RealMe',
            'prod_id'=>2
        ]);
        ProductsOfProduct::create([
            'name' => 'Boat',
            'prod_id'=>2
        ]);
    }
}
