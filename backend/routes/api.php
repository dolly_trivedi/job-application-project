<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ApplicantsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/employee',[EmployeeController::class,'getData']);
Route::post('/addemp',[EmployeeController::class,'addEmployeeData']);
Route::delete('/deleteemp/{id}',[EmployeeController::class,'deleteEmployee']);
Route::put('/editemp/{id}',[EmployeeController::class,'editEmployee']);
Route::get('/employee/{id}',[EmployeeController::class,'getDataById']);
Route::get('/dropdown-demo',[EmployeeController::class,'getProducts']);




Route::post('/login',[AdminController::class,'postLogin']);
Route::group(['middleware' => ['JWTAuth']], function() {
    Route::get('/employee',[EmployeeController::class,'getData']);
});
Route::post('/add-applicants',[ApplicantsController::class,'addApplicants']);
Route::post('/add-experience/{id}',[ApplicantsController::class,'addWorkExperience']);

Route::delete('/delete-applicant/{id}',[ApplicantsController::class,'deleteApplicant']);
Route::get('/applicant-list',[ApplicantsController::class,'getAllApplicants']);
Route::get('/applicant/{id}',[ApplicantsController::class,'getApplicantById']);